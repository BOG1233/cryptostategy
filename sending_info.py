import re
import os
from config import url_to_send, mask, token, directory
import socket
import requests
from time import sleep
import json


def send_info():
    pathes = find_files()

    for path in pathes:
        body = {}
        file = open(path, 'r')
        body['info'] = file.read()
        file.close()
        table = re.sub(r'[^\w+]', '', str(socket.gethostname()))

        body['server_name'] = table
        body['server_ip'] = socket.gethostbyname(socket.gethostname())
        body['file_path'] = path
        body['token'] = token
        #print(json.dumps(body))
        response = requests.post(url_to_send, json=body)
        try:
            rez = response.json()
        except Exception as err:
            print(f'JsonError {err}')
            continue

        print(rez)
        if rez.get('code') == 200:
            continue
        elif rez.get('code') == 400:
            continue


def find_files():
    pathes = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(mask):
                path = os.path.join(root, file)
                path = re.sub(r'\\', "/", path)
                pathes.append(path)
    print(pathes)
    return pathes


def main():
    table = re.sub(r'[^\w+]', '', str(socket.gethostname()))
    rez = requests.get(url_to_send+table)
    print(rez.text)
    while True:
        try:
            send_info()
            print('completed')
            sleep(60*60*24)
        except Exception as err:
            print(err)


main()

