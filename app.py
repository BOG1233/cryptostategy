from flask import Flask, jsonify, request
from src.DataBase import Database
from config import tokens

app = Flask(__name__)


# routes
@app.route('/api/v0/post_info/', methods=['POST'])
def post_info():
    """
    response body {
        'info': str,
        'server_name': str,
        'server_ip': str,
        'token': str}
    :return: 0
    """
    if not request.json:
        return jsonify({'code': 452, 'desc': 'Not JSON'})
    else:
        body = request.json
        if body.get('token') in tokens:
            if body.get('server_name'):
                if body.get('server_ip'):
                    if body.get('file_path'):
                        db = Database()
                        is_added = db.add_info(body.get('server_name'), body.get('info'),
                                               body.get('server_ip'), body.get('file_path'))
                        db.conn.close()
                        if is_added:
                            return jsonify({'code': 200, 'desc': "Ok"})
                        else:
                            return jsonify({'code': 300, 'desc': "Database Error"})
        return jsonify({'code': 300, 'desc': 'invalid body'})


@app.route('/api/v0/post_info/<table_name>', methods=['GET'])
def get_info(table_name):
    db = Database()
    created = db.create_server_table(table_name)
    db.conn.close()
    if created:
        return jsonify({'code': 200, 'desc': 'Table created'})
    else:
        return jsonify({'code': 300, 'desc': 'Table was exist'})


def start_app():
    app.run()

