import psycopg2
from config import db_name, time_zone
from datetime import datetime


class Database:

    def __init__(self):
        try:
            self.conn = psycopg2.connect(db_name)
            self.cursor = self.conn.cursor()
        except psycopg2.Error as err:
            print(f'Connection Error: {err}')

    def create_server_table(self, table_name):
        try:
            self.cursor.execute(f"""CREATE TABLE info_{table_name} (id serial PRIMARY KEY,
                                                               time integer,
                                                               info text NULL,
                                                               server_ip varchar(30),
                                                               file_path varchar(200))""")
            self.conn.commit()
            return True
        except psycopg2.Error:
            print(f"Table {table_name} was created")
            return False

    def add_info(self, table_name, text, server_ip, file_path):
        try:
            time_created = int(datetime.utcnow().timestamp())

            self.cursor.execute(f"""INSERT INTO info_{table_name}(time, info, server_ip, file_path) 
                                 VALUES ({time_created}, '{text}', '{server_ip}', '{file_path}')""")
            self.conn.commit()
        except psycopg2.Error as err:
            print(f'Query error: {err}')
            return False
        return True

    def select_info_per_time(self, table_name, time_to_select=1):
        cur_time = datetime.utcnow().timestamp() - time_to_select * 60 + time_zone * 60
        self.cursor.execute(f"""SELECT * FROM info_{table_name} WHERE time >= {int(cur_time)}""")
        return self.cursor.fetchall()

    def __del__(self):
        self.conn.close()
        del self.cursor, self.conn





